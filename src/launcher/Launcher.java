package launcher;

import entity.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

public class Launcher {
    
    public static void main(String args[]) throws IOException, InterruptedException{
        
        
        BufferedReader userInputMain=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter your name:- ");
        String name = userInputMain.readLine();
        Socket socket = null;
        ServerSocket serverSocket = null;
        try{//He is server
            serverSocket = new ServerSocket(58000);
            serverSocket.setSoTimeout(60000);
            System.out.println("Waiting for another user to connect");
            socket = serverSocket.accept();
            System.out.println("User connected. You can start chating.");
        }catch(BindException bindException){//He is client
            socket = new Socket("localhost", 58000);
            System.out.println("Please start chating.");
        }catch(Exception ex){
            System.out.println("Sorry All channels busy. Please try later.");
        }
        
        Entity entity = new Entity(socket,name);
        entity.fire();
    }
}